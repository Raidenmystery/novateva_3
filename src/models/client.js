'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClientSchema = Schema
({
    Name: 
    {
        type: String,
        require: true
    },
    Id:  
    {
        type: Number,
        unique: true,
        require: true
    },
    Age: 
    {
        type: Number,
        require: true
    },
    Gender: 
    {
        type: String,
        enum: ['female','male'],
        require: true
    },
    Date: 
    {
        type: Date,
        require: true
    },
    Address: 
    {
        type: String,
        require: true
    },
});

module.exports = mongoose.model('Client', ClientSchema);