'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdminSchema = Schema
({
    Name: 
    {
        type: String,
        require: true
    },
    Id: 
    {
        type: Number,
        unique: true,
        require: true
    },
    Age: 
    {
        type: Number,
        require: true
    },
    Gender: 
    {
        type: String,
        enum: ['female','male'],
        require: true
    },
    Date: 
    {
        type: Date,
        require: true
    },
    Address: 
    {
        type: String,
        require: true
    },
    User: 
    {
        type: String,
        unique:true,
        require: true
    },
    Password:
    {
        type: String,
        require: true
    },
    Email:
    {
        type: String,
        unique:true
    }
});

module.exports = mongoose.model('Admin', AdminSchema);