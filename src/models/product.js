'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = Schema
({
    Name: 
    {
        type: String,
        require: true
    },
    Amount: 
    {
        type: Number,
        require: true
    },
    Category:
    {
        type: String,
        enum: ['vegetable','fruit','sausage','meat'],
        require: true
    },
    Price: 
    {
        type: Number,
        require: true
    },
});

module.exports = mongoose.model('Product', ProductSchema);