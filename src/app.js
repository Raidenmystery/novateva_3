'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const api_admin = require('./routes/admin.js');
const api_client = require('./routes/client.js');
const api_product = require('./routes/product.js');
const api_login = require('./routes/login');
const cors = require('cors');

//Middleware
app.use(bodyParser.urlencoded(
  {
     extended: false
  }));
app.use(bodyParser.json());
app.use(cors());

app.use('/api',api_admin);
app.use('/api',api_client);
app.use('/api',api_product);
app.use('/api',api_login);

module.exports = app;