'use strict'

const express = require('express');
const LoginCtrl = require('../controllers/login.js');
const api_login = express.Router();

api_login.get('/login', LoginCtrl.getLogin);

module.exports = api_login;