'use strict'

const express = require('express');
const AdminCtrl = require('../controllers/admin.js');
const api_admin = express.Router();

api_admin.get('/admin/:AdminId', AdminCtrl.getAdmin);
api_admin.get('/admin', AdminCtrl.getAdmins);
api_admin.post('/admin', AdminCtrl.saveAdmin);
api_admin.put('/admin/:AdminId', AdminCtrl.updateAdmin);
api_admin.delete('/admin/:AdminId', AdminCtrl.deleteAdmin);

module.exports = api_admin;