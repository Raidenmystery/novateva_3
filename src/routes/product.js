'use strict'

const express = require('express');
const ProductCtrl = require('../controllers/product.js');
const api_product = express.Router();

api_product.get('/product/:ProductId', ProductCtrl.getProduct);
api_product.get('/product', ProductCtrl.getProducts);
api_product.post('/product', ProductCtrl.saveProduct);
api_product.put('/product/:ProductId', ProductCtrl.updateProduct);
api_product.delete('/product/:ProductId', ProductCtrl.deleteProduct);

module.exports = api_product;