'use strict'

const Product = require('../models/product.js');
const crypt = require('bcrypt');
const validator = require('validator');

function getProduct (req, res)
{
    let ProductId = req.params.ProductId;

    Product.findById(ProductId, (err, Product) =>
    {
        if (err) return res.status(500).send({message: `Error al realizar la peticion : ${err}`});

        if (!Product) return res.status(404),send({ message: `El producto no existe`});

        res.status(200).send({ Product: Product});

    });
}

function getProducts (req, res)
{
    Product.find({}, (err, Products) => 
      {
        if (err) return res.status(500).send(
          {
             message: `Error al realizar la peticion : ${err}`
          });

        if (!Products) return res.status(404).send(
          {
             message: 'No existen productos'
          });

        res.status(200).send(
          {
             Products: Products
          });

      });
}

function saveProduct (req, res) 
{
    //console.log('\nPost /api/product\n');
    //console.log(req.body);

    let product = new Product(req.body);
    
    product.Amount = Number(product.Amount);
    product.Price = Number(product.Price);

    if ( product.Amount < 1) res.status(400).send( {message: `\nError al salvar en la base de datos: Cantidad no valida ${product.Amount}`});

    if ( product.Price < 1) res.status(400).send( {message: `\nError al salvar en la base de datos: Precio no valido ${product.Price}`});

    product.save((err, productRegistered) => 
    {
        if (err) res.status(500).send( {message: `\nError al salvar en la base de datos: ${err}`});

        res.status(200).send({product: productRegistered});

    });
}

function updateProduct (req, res) 
{
    let ProductId = req.params.ProductId;
    let update = new Product(
        {
            _id:ProductId,
            Name: req.body.Name,
            Amount: Number(req.body.Amount),
            Category: req.body.Category,
            Price: Number(req.body.Price)
        });

    if ( update.Amount < 1) res.status(400).send( {message: `\nError al salvar en la base de datos: Cantidad no valida ${update.Amount}`});

    if ( update.Price < 1) res.status(400).send( {message: `\nError al salvar en la base de datos: Precio no valido ${update.Price}`});

    Product.findByIdAndUpdate(ProductId, update,
        {
            context: 'query',
            new: true,
            runValidators: true
        }, (err, ProductUpdated) => 
    {
        if (err) res.status(500).send({message: `Error al actualizar el producto: ${err}`});

        res.status(200).send({ Product: ProductUpdated });
    });

}

function deleteProduct (req, res) 
{
    let ProductId = req.params.ProductId;

    Product.findById((ProductId), (err, Product) => 
    {
        if (err) res.status(500).send({message: `Error al borrar el product: ${err}`});

        Product.remove(err => 
        {
            if (err) res.status(500).send({message: `Error al borrar el producto: ${err}`});
            res.status(200).send({message: `El producto ha sido eliminado`});            
        });
    })
}


module.exports = 
{
    getProduct,
    getProducts,
    saveProduct,
    updateProduct,
    deleteProduct
}