'use strict'

const Admin = require('../models/admin.js');
const crypt = require('bcrypt');
const validator = require('validator');

function getAdmin (req, res)
{
    let AdminId = req.params.AdminId;

    Admin.findById(AdminId, (err, Admin) =>
    {
        if (err) return res.status(500).send({message: `Error al realizar la peticion : ${err}`});

        if (!Admin) return res.status(404),send({ message: `El trabajador no existe`});

        res.status(200).send({ Admin: Admin});

    });
}

function getAdmins (req, res)
{
    Admin.find({}, (err, Admins) => 
      {
        if (err) return res.status(500).send(
          {
             message: `Error al realizar la peticion : ${err}`
          });

        if (!Admins) return res.status(404).send(
          {
             message: 'No existe personal administrativo'
          });

        res.status(200).send(
          {
             Admins: Admins
          });

      });
}

function saveAdmin (req, res)
{
    //console.log('\nPost /api/admin\n');
    //console.log(req.body);

    let admin = new Admin(req.body);

    admin.Age = Number(admin.Age);
    admin.Id = Number(admin.Id);
    admin.Password = crypt.hashSync(admin.Password,10);

    if(!validator.isEmail(admin.Email)) res.status(400).send( {message: `\nError al salvar en la base de datos: Correo no valido ${admin.Email}`});

    if (admin.Age <= 18) res.status(400).send( {message: `\nError al salvar en la base de datos: Edad no valida ${admin.Age}`});

    admin.save((err, adminRegistered) => 
    {
        if (err) res.status(500).send( {message: `\nError al salvar en la base de datos: ${err}`});

        res.status(200).send({admin: adminRegistered});
    });
    
}

function updateAdmin (req, res) 
{
    let AdminId = req.params.AdminId;
    let update = new Admin (
        {
            _id: AdminId,
            Name: req.body.Name,
            Id: req.body.Id,
            Age: req.body.Age,
            Gender: req.body.Gender,
            Date: req.body.Date,
            Address: req.body.Address,
            User: req.body.User,
            Password: req.body.Password,
            Email: req.body.Email
        });

        update.Password = crypt.hashSync(update.Password,10);   
    
    Admin.findByIdAndUpdate(AdminId, update,
        {
            context: 'query',
            new: true,
            runValidators: true
        }, (err, AdminUpdated) => 
    {
        if (err) res.status(500).send({message: `Error al actualizar el empleado: ${err}`});

        res.status(200).send({ Admin: AdminUpdated });
    });

}

function deleteAdmin (req, res) 
{
    let AdminId = req.params.AdminId;

    Admin.findById((AdminId), (err, Admin) => 
    {
        if (err) res.status(500).send({message: `Error al borrar el empleado: ${err}`});

        Admin.remove(err => 
        {
            if (err) res.status(500).send({message: `Error al borrar el empleado: ${err}`});
            res.status(200).send({message: `El empleado ha sido eliminado`});            
        });
    })
}

module.exports = 
{
    getAdmin,
    getAdmins,
    saveAdmin,
    updateAdmin,
    deleteAdmin
}