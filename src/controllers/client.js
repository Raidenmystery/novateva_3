'use strict'

const Client = require('../models/client.js');
const crypt = require('bcrypt');
const validator = require('validator');

function getClient (req, res)
{
    let ClientId = req.params.ClientId;

    Client.findById(ClientId, (err, Client) =>
    {
        if (err) return res.status(500).send({message: `Error al realizar la peticion : ${err}`});

        if (!Client) return res.status(404),send({ message: `El cliente no existe`});

        res.status(200).send({ Client: Client});

    });
}

function getClients (req, res)
{
    Client.find({}, (err, Clients) => 
      {
        if (err) return res.status(500).send(
          {
             message: `Error al realizar la peticion : ${err}`
          });

        if (!Clients) return res.status(404).send(
          {
             message: 'No existen clientes'
          });

        res.status(200).send(
          {
             Clients: Clients
          });

      });
}

function saveClient (req, res) 
{
    //console.log('\nPost /api/product\n');
    //console.log(req.body);

    let client = new Client(req.body);

    client.Age = Number(client.Age);
    client.Id = Number(client.Id);

    if (client.Age <= 10) res.status(400).send( {message: `\nError al salvar en la base de datos: Edad no valida ${client.Age}`});

    client.save((err, clientRegistered) => 
    {
        if (err) res.status(500).send( {message: `\nError al salvar en la base de datos: ${err}`});

        res.status(200).send({client: clientRegistered});

    });
}

function updateClient (req, res) 
{
    let ClientId = req.params.ClientId;
    let update = new Client (
        {
            _id: ClientId,
            Name: req.body.Name,
            Id: req.body.Id,
            Age: req.body.Age,
            Gender: req.body.Gender,
            Date: req.body.Date,
            Address: req.body.Address
        });

    Client.findByIdAndUpdate(ClientId, update,
        {
            context: 'query',
            new: true,
            runValidators: true
        },(err, ClientUpdated) => 
    {
        if (err) res.status(500).send({message: `Error al actualizar el cliente: ${err}`});

        res.status(200).send({ Client: ClientUpdated });
    });

}

function deleteClient (req, res) 
{
    let ClientId = req.params.ClientId;

    Client.findById((ClientId), (err, Client) => 
    {
        if (err) res.status(500).send({message: `Error al borrar el cliente: ${err}`});

        Client.remove(err => 
        {
            if (err) res.status(500).send({message: `Error al borrar el cliente: ${err}`});
            res.status(200).send({message: `El cliente ha sido eliminado`});            
        });
    })
}


module.exports = 
{
    getClient,
    getClients,
    saveClient,
    updateClient,
    deleteClient
}