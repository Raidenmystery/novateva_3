const express = require('express');
const bcrypt = require('bcrypt');
const Admin = require('../models/admin');

function getLogin(req, res) {
    let login = req.body;

    if (login.User === undefined || login.Password === undefined)
        return res.status(400).send({
            message: `Campos vacios`
        });

    Admin.findOne({User: login.User},(err,user) =>
    {
        if(err) return res.status(500).send(
        {
            message: `Error al buscar en la base de datos: ${err}`
        });

        if(!user) return res.status(500).send
        ({
            message: `Usuario o contraseña invalidos`
        });

        if( ! bcrypt.compareSync(login.Password,user.Password)) 
        {
            return res.status(400).send
            ({
                    message: "Usuario o contraseña invalida"
            });
        }

        res.json({Admin:user});

    });
}

module.exports = 
{
    getLogin
}