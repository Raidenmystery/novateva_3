import React, { Component } from 'react'

export class footer extends Component {
  render() {
    return (
       <footer class="page-footer font-small bg-dark text-light ml-5 mr-5 shadow-lg">
           <div class="footer-copyright text-center py-3">Diseñado por Rafael Rivas ||
             <a href="/" className="text-light"> Page</a>
           </div>
       </footer>
    )
  }
}

export default footer
