import React, { Component } from 'react'
import Card from './card';
import arrowright from '../img/next.png'
import arrowleft from '../img/back.png'

export class main extends Component {
  render() {
    return (
      <div className="container mt-5 mb-5 " id="ppalcarousel">        
        <div id="carouselExampleIndicators" className="carousel slide center" data-ride="carousel" data-interval="false">
        <div className="carousel-inner center col 10">
              <div className="carousel-item active">
                <Card head='¡Aqui podras registrarte y tambien acceder!' title='1' description='Primero'/>
              </div>
              <div className="carousel-item">
                <Card head='¡Aqui podras registrar  clientes!' title='2' description='Segundo'/>
              </div>
              <div className="carousel-item">
                <Card head='¡Aqui podras registrar productos!' title='3' description='Tercero'/>
              </div>
            </div>
            <a className="carousel-control-prev col-1" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <img src={arrowleft} alt="arrowleft" />
            </a>            
            <a className="carousel-control-next col-1" href="#carouselExampleIndicators" role="button" data-slide="next">
            <img src={arrowright} alt="arrowright" />
            </a>
        </div>
        
      </div>
    )
  }
}

export default main
