import React, { Component } from 'react'
import Adminlogin from './adminlogin'

export class card extends Component {
  
  render() {
    return (
        <div className="container">
            <div className="card text-center border border-primary ">
              <div className="bg-dark p-3 text-light">
                <strong>
                  {this.props.head}
                </strong>
              </div>
              <div className="card-body">
                <h4 className="card-title">{this.props.title}</h4>
                <p className="card-text">{this.props.description}</p>
                <Adminlogin/>
              </div>
            </div>           
        </div>
    )
  }
}

export default card
