import React, { Component } from 'react'
import image from '../img/store.png'

export class header extends Component {
  render() {
    return (
      <div className="sticky-top">
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg container shadow-lg">
          <img src={image} alt="icon"/>
          <a className="navbar-brand m-auto" href="/">
            {this.props.title}
          </a>
          <img src={image} alt="icon" />
     </nav>
  </div>
    )
  }
}

export default header
