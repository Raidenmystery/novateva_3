import React, { Component } from 'react';
import Header from './components/header';
import Footer from './components/footer';
import Main from './components/main';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Header
          title = {'Shop'}
        />
        <Main/>
        <Footer/>
      </React.Fragment>
    );
  }
}

export default App;
